<?php
class Criteo_OneTag_Helper_Feed extends Mage_Core_Helper_Abstract {
    protected $_list = null;
	protected $_ctoIdType = null;
 
    public function __construct() {
        $collection = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*');
        $collection->addFieldToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH);
        $this->setList($collection);
    }
 
    public function setIdType($ctoIdType){
        $this->_ctoIdType = $ctoIdType;
    }
 
    public function setList($collection){
        $this->_list = $collection;
    }
 
    public function generateFeed() {
        if (is_null($this->_list)) {        	
        	$this->showError("Could not retrieve any products from the catalog!");
        } else {
			//Get settings
			$ctoFeedActivated = Mage::helper('Criteo_OneTag')->get_feed_activated();
			$ctoFeedPassword = Mage::helper('Criteo_OneTag')->get_feed_password();
			$this->setIdType(Mage::helper('Criteo_OneTag')->get_product_id());
            
            // Activation check
            if(!$ctoFeedActivated) {
            	$this->showError("Feed generation deactivated in configuration panel!");
            } else {
				//Password checks
				if(strlen($ctoFeedPassword) > 0 && filter_input(INPUT_GET, 'p') == null) {
	            	$this->showError("Access denied. Feed is protected by password.");
				} else {
					if(filter_input(INPUT_GET, 'p', FILTER_SANITIZE_STRING) != $ctoFeedPassword) {
	            		$this->showError("Password provided does not match password set in configuration panel!");
					} else {
						//Get items in our shop
					    $items = $this->_list;
						
						//Display the header with correct names depending on the ID configuration
						if($this->_ctoIdType == 0) {
							echo "id|sku|name|producturl|smallimage|bigimage|description|price|retailprice|instock|categoryid1|categoryid2|categoryid3";
						} else {
							echo "id|magentoid|name|producturl|smallimage|bigimage|description|price|retailprice|instock|categoryid1|categoryid2|categoryid3";
						}
						Mage::getSingleton('core/resource_iterator')->walk($items->getSelect(), array(array($this, 'printProduct')));
					}
				}
			}
        }
    }
 
	// gathers all info, formats the product line and prints it
    public function printProduct($args){
		$i = Mage::getModel('catalog/product')->loadByAttribute('sku', $args['row']['sku']);
		
		// line break
		$printLine = "\n";

		//ID - selects the id based on whether they use SKU or not in the configuration and passes the other ID type for convenience
		if($this->_ctoIdType == 0) {
			$printLine .= $i->getEntityId()."|".$i->getSku()."|";
		} else {
			$printLine .= $i->getSku()."|".$i->getEntityId()."|";
		}

		//Name
		$printLine .= htmlentities($i->getName(), ENT_QUOTES)."|";
		
		//Product url
		$printLine .= Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB).$i->getUrlPath()."|";
		
		//Small image
		$smallimage = $i->getSmallImage();
		if($smallimage == "no_selection") {
			//We don't want to add no_selection in our feed
			$printLine .= "|";
		} else {
			$printLine .= Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA)."catalog/product".$smallimage."|";
		}
		
		//Big image
		$bigimage = $i->getImage();
		if($bigimage == "no_selection") {
			//We don't want to add no_selection in our feed
			$printLine .= "|";
		} else {
			$printLine .= Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA)."catalog/product".$bigimage."|";
		}
		
		//Description
		$printLine .= str_replace(
			array("\n","\r\n","\r"), 
			'', 
			htmlentities($i->getDescription(), ENT_QUOTES)
		)."|";

		//Prices
		$price = $i->getPrice();
		$specialprice = $i->getSpecialPrice();
		$finalprice = $i->getFinalPrice();
		if ($specialprice != "") {
			//We have a retail price
			$printLine .= $specialprice."|".$price."|";
		} else if ($finalprice != "") {
			//We have a retail price
			$printLine .= $finalprice."|".$price."|";
		} else {
			//No retail price
			$printLine .= $price."||";
		}
		
		//Instock - isAvailable is used to mark if something is in stock or not
		$printLine .= $i->isAvailable()."|";
		
		//Categories - goes as deep as possible, and then back up to the next root category
		$categories = $i->getCategoryIds();
		$categories_counter = 0;
		foreach($categories as $c) {
			$category = Mage::getModel('catalog/category')->load($c);
			if($categories_counter < 3 && $category->getIsActive()) {
				$printLine .= ($categories_counter == 0 ? "" : "|").$category->getName();
			}
			$categories_counter++;
		}
		while($categories_counter < 3) {
			$printLine .= "|";
			$categories_counter++;
		}
		
		// insert product in the feed
		echo $printLine;
    }
 
    public function showError($error){
    	$html = "
    		<style>
    			.error {
					margin: 10px 0px;
					padding:12px;
				    color: #D8000C;
				    background-color: #FFBABA;
				}
    		</style>
    		<div class='error'>
    			<strong>ERROR:</strong> $error
    		</div>
    	";
    	echo $html;
    }
}
?>